package ro.ubb.catalog.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ro.ubb.catalog.web.converter.BookConverter;
import ro.ubb.catalog.web.dto.BookDto;
import ro.ubb.catalog.web.dto.BooksDto;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {

    private static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private BookConverter bookConverter;

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public BooksDto getBooks() {
        log.trace("getBooks");

        List<Book> books = bookService.getAllBooks();

        log.trace("getBooks: books={}", books);

        return new BooksDto(bookConverter.convertModelsToDtos(books));
    }

    @RequestMapping(value = "/books/filter", method = RequestMethod.GET)
    public BooksDto filterByTitle(@RequestParam(value="title", required=true) String title) {
        log.trace("filterByTitle");

        List<Book> books = bookService.getAllBooks();

        ArrayList<Book> filteredBooks = new ArrayList<>();
        books.forEach(filteredBooks::add);
        filteredBooks.removeIf(book -> !book.getTitle().contains(title));

        log.trace("getBooks: books={}", books);

        return new BooksDto(bookConverter.convertModelsToDtos(filteredBooks));
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto saveBook(@RequestBody BookDto dto) {
        log.trace("saveBook: dto={}", dto);

        Book saved = this.bookService.saveBook(
                bookConverter.convertDtoToModel(dto)
        );
        BookDto result = bookConverter.convertModelToDto(saved);

        log.trace("saveBook: result={}", result);

        return result;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.PUT)
    BookDto updateBook(@PathVariable Long id,
                             @RequestBody BookDto dto) {
        log.trace("updateBook: id={},dto={}", id, dto);

        Book update = bookService.updateBook(
                id,
                bookConverter.convertDtoToModel(dto));
        BookDto result = bookConverter.convertModelToDto(update);

        return result;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteBook(@PathVariable Long id) {
        log.trace("deleteBook: id={}", id);

        bookService.deleteBook(id);

        log.trace("deleteStudent --- method finished");

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
