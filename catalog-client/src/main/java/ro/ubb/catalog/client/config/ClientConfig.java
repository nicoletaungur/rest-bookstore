package ro.ubb.catalog.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import ro.ubb.catalog.client.Console;
import ro.ubb.catalog.web.converter.BookConverter;

@Configuration
public class ClientConfig {
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    BookConverter bookConverter(){ return new BookConverter();}

    @Bean
    Console console() {return new Console();}
}
