package ro.ubb.catalog.client.command.book_command;

import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ro.ubb.catalog.client.command.Command;
import ro.ubb.catalog.web.converter.BookConverter;
import ro.ubb.catalog.web.dto.BooksDto;

import java.util.Scanner;

public class FilterByTitleCommand extends Command {
    @Override
    public String getName() {
        return "Filter books by title.";
    }

    @Override
    public void run(RestTemplate restTemplate, BookConverter bookConverter) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Filter by title: ");
        String title = scanner.nextLine();
        try {
            BooksDto dto = restTemplate.getForObject(String.format("http://localhost:8080/api/books/filter?title=%s", title), BooksDto.class);

            if (dto != null) {
                dto.getBooks().forEach(x -> {
                    System.out.println(bookConverter.convertDtoToModel(x));
                });
            } else {
                System.err.println("No books found. Try adding some.");
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
